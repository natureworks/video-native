1
00:00:00,600 --> 00:00:02,766
Greetings and welcome and this is a

2
00:00:02,800 --> 00:00:05,300
bonus session for all my Udemy students

3
00:00:05,533 --> 00:00:08,333
This is a section on native plants

4
00:00:08,666 --> 00:00:10,366
A  couple of things before I get started

5
00:00:10,366 --> 00:00:12,966
Firstly, this is my website here

6
00:00:13,000 --> 00:00:15,166
natureworks.org.uk

7
00:00:15,166 --> 00:00:18,300
Secondly, the slideshow is always online at

8
00:00:18,300 --> 00:00:22,633
this url natureworks.org.uk/talks/native

9
00:00:22,933 --> 00:00:24,433
and there are a few notes in

10
00:00:24,433 --> 00:00:25,466
here, if you hold down the p

11
00:00:25,466 --> 00:00:27,133
key you will see the notes on

12
00:00:27,133 --> 00:00:28,333
the right-hand side

13
00:00:28,400 --> 00:00:30,166
Fundamental takeaway here really is

14
00:00:30,166 --> 00:00:32,433
you grow native plants because they

15
00:00:32,433 --> 00:00:35,433
have co-evolved with the native wildlife

16
00:00:35,666 --> 00:00:39,333
An outline. Why native plants? Whereabouts should you

17
00:00:39,333 --> 00:00:40,333
put the native plants

18
00:00:40,333 --> 00:00:44,400
in your garden. Reveling in the vernacular.

19
00:00:44,400 --> 00:00:47,033
All plants are useful and then

20
00:00:47,033 --> 00:00:50,466
finally looking at cultivars nativars and near-natives

21
00:00:50,466 --> 00:00:53,100
Why native plants?

22
00:00:54,366 --> 00:00:57,566
Now, a very brief description and definition of

23
00:00:57,566 --> 00:01:00,966
a forest garden is that it's an edible ecosystem.

24
00:01:00,966 --> 00:01:02,733
Your forest garden is providing it's

25
00:01:02,800 --> 00:01:07,133
own natural pest-control. You're inviting in all

26
00:01:07,133 --> 00:01:08,866
the predators to eat all the bugs

27
00:01:08,866 --> 00:01:11,200
and keep a balanced ecosystem and you

28
00:01:11,200 --> 00:01:13,133
also have backed into that forest garden

29
00:01:13,133 --> 00:01:14,400
is the fertility as well

30
00:01:14,400 --> 00:01:16,666
You want to include the native

31
00:01:16,666 --> 00:01:19,266
plants in your forest garden because they

32
00:01:19,266 --> 00:01:21,700
have co-evolved with the wildlife and they're

33
00:01:21,700 --> 00:01:26,266
providing the foodstuff for the invertebrates and

34
00:01:26,266 --> 00:01:29,833
invertebrates are the backbone of ecosystems.

35
00:01:30,000 --> 00:01:31,633
So you have the the plants that they

36
00:01:31,633 --> 00:01:34,133
feed upon and then you have the invertebrates

37
00:01:34,400 --> 00:01:37,233
and they are in turn being eaten as

38
00:01:37,233 --> 00:01:38,800
part of the food web and they're

39
00:01:38,800 --> 00:01:42,766
supporting other animals, it fits in together

40
00:01:42,766 --> 00:01:45,700
so it's really really important to have

41
00:01:45,733 --> 00:01:48,166
native plants as part of that mix

42
00:01:48,466 --> 00:01:51,200
For an example looking at a

43
00:01:51,200 --> 00:01:54,400
Japanese Maple, this is Acer japonicum

44
00:01:54,766 --> 00:01:57,766
versus a UK native Hawthorn

45
00:01:57,766 --> 00:02:01,900
Crataegus monogyna and the Acer japonicum

46
00:02:01,933 --> 00:02:03,766
comes from Japan apparently and South Korea

47
00:02:03,866 --> 00:02:06,266
and the Crataegus monogyna, it's distribution

48
00:02:06,266 --> 00:02:08,900
is across Europe, northwest Africa and west

49
00:02:08,900 --> 00:02:13,400
Asia and what's useful here is to

50
00:02:13,400 --> 00:02:14,866
look in the uk

51
00:02:14,866 --> 00:02:19,233
at what invertebrates are using those trees

52
00:02:19,266 --> 00:02:21,200
as a host plant so what is

53
00:02:21,200 --> 00:02:23,933
a eating those plants. And a brilliant

54
00:02:23,933 --> 00:02:26,066
resource for this is DataBase of Insects

55
00:02:26,066 --> 00:02:28,666
and their Food plants and if you

56
00:02:28,666 --> 00:02:32,300
look at the Japanese Maple this has a

57
00:02:32,300 --> 00:02:35,033
total number of interactions of just one!

58
00:02:35,033 --> 00:02:37,066
There's one known interaction, there

59
00:02:37,066 --> 00:02:38,200
will be more but this is the

60
00:02:38,200 --> 00:02:40,800
one scientific study that has

61
00:02:40,800 --> 00:02:44,033
shown an interaction with UK native invertebrates

62
00:02:44,366 --> 00:02:45,766
with a Japanese Maple

63
00:02:47,000 --> 00:02:49,966
And you compare that on the DataBase

64
00:02:49,966 --> 00:02:52,166
of Insects and their food plants website to

65
00:02:52,166 --> 00:02:53,166
the Hawthorn

66
00:02:54,233 --> 00:02:56,700
it's a totally totally different story

67
00:02:56,700 --> 00:03:00,500
A hundred and ninety seven interactions and this

68
00:03:00,566 --> 00:03:02,733
remember is the insects, this is just

69
00:03:02,733 --> 00:03:05,766
the invertebrates which are using the plant

70
00:03:05,766 --> 00:03:08,066
as a host plant. This doesn't take

71
00:03:08,066 --> 00:03:12,600
into account mammals and amphibians and

72
00:03:12,600 --> 00:03:14,366
all sorts of other all sorts of creatures

73
00:03:14,400 --> 00:03:15,766
and the fungi and all the other

74
00:03:15,766 --> 00:03:17,300
interactions that are going on as well

75
00:03:17,466 --> 00:03:19,100
and how it's being used for habitat

76
00:03:19,166 --> 00:03:22,366
The Hawthorn is one of the best native

77
00:03:22,366 --> 00:03:23,800
plants that you can put into your

78
00:03:23,800 --> 00:03:25,366
garden so if you're going to plant

79
00:03:25,366 --> 00:03:27,700
one tree for wildlife in your garden

80
00:03:27,766 --> 00:03:29,366
it should be a hawthorn and they are

81
00:03:29,366 --> 00:03:30,966
amazing plants as well and they're really

82
00:03:30,966 --> 00:03:32,666
easy to train, they look stunning, they

83
00:03:32,666 --> 00:03:34,566
have incredible blossom, edible

84
00:03:34,566 --> 00:03:36,866
haws and they're just amazing where

85
00:03:37,100 --> 00:03:39,500
Where to put your native plants? In a

86
00:03:39,500 --> 00:03:41,866
forest garden obviously there's lots of non-native

87
00:03:41,866 --> 00:03:44,000
plants in there as well. The bulk

88
00:03:44,000 --> 00:03:45,633
of the garden, what you're aiming for,

89
00:03:45,633 --> 00:03:47,833
you want to get that biomass bulk

90
00:03:47,833 --> 00:03:49,066
youi want to get a diversity of

91
00:03:49,066 --> 00:03:50,833
native plants bu you also want to

92
00:03:50,833 --> 00:03:52,966
get that bulk. There's three

93
00:03:52,966 --> 00:03:54,733
areas where you can do this

94
00:03:54,833 --> 00:03:57,000
so windbreaks, rather than using a

95
00:03:57,000 --> 00:04:00,100
non-native windbreak, for example an Autumn

96
00:04:00,100 --> 00:04:03,733
Olive or a Rosa rugosa, you use

97
00:04:03,766 --> 00:04:07,066
a native windbreak, for example Guelder

98
00:04:07,066 --> 00:04:11,300
Rose or Sea Buckthorn. Where you can, use

99
00:04:11,300 --> 00:04:13,066
native plants for windbreaks and there's actually

100
00:04:13,066 --> 00:04:14,866
quite a few different choices. This

101
00:04:14,933 --> 00:04:17,133
is a Guelder Rose and also again Guelder

102
00:04:17,133 --> 00:04:20,866
Rose, Viburnum opulus, absolutely glorious plant

103
00:04:20,866 --> 00:04:22,633
as well but it's a proper ornamental

104
00:04:22,633 --> 00:04:25,700
garden plant kind of edible, really very

105
00:04:25,700 --> 00:04:27,766
useful and very very beautiful, beautiful flowers

106
00:04:27,766 --> 00:04:29,700
and beautiful berries and beautiful autumn color

107
00:04:29,833 --> 00:04:32,266
on the leaves too. The other

108
00:04:32,266 --> 00:04:33,466
area that you want to look

109
00:04:33,466 --> 00:04:35,033
at using native plants is

110
00:04:35,100 --> 00:04:38,466
ground cover. So here I have a Ground Ivy

111
00:04:38,466 --> 00:04:40,633
actually propagating ground ivy which some

112
00:04:40,633 --> 00:04:42,633
people call a weed but it's actually

113
00:04:42,633 --> 00:04:44,566
very very useful plant to have about

114
00:04:44,566 --> 00:04:46,533
the place, this is Glechoma hederacea

115
00:04:46,533 --> 00:04:49,133
and in the UK

116
00:04:49,133 --> 00:04:51,400
likes kind of damp part shade

117
00:04:51,433 --> 00:04:53,800
conditions good to have in those areas

118
00:04:54,166 --> 00:04:55,200
and like every other plant

119
00:04:55,300 --> 00:04:57,500
you want to get a mosaic to

120
00:04:57,500 --> 00:04:59,600
get the plants where they're happiest so

121
00:05:00,166 --> 00:05:03,600
putting different native plants into different areas

122
00:05:03,633 --> 00:05:05,533
The final plant that you want to

123
00:05:05,533 --> 00:05:07,566
be you want your native plants is

124
00:05:07,566 --> 00:05:10,100
system plants and these are plants which are

125
00:05:10,100 --> 00:05:11,666
providing other kind of functions so you

126
00:05:11,666 --> 00:05:13,666
have like, yeah, wind breaks essentially

127
00:05:13,666 --> 00:05:15,233
are system, ground cover is system, like

128
00:05:15,466 --> 00:05:19,466
but also fertility so there are quite

129
00:05:19,466 --> 00:05:22,133
a few nitrogen fixing native plants, members

130
00:05:22,133 --> 00:05:25,333
of the pea family, Scotch Broom of

131
00:05:25,366 --> 00:05:29,533
Everlasting Pea there's also the

132
00:05:29,533 --> 00:05:31,433
Sea Buckthorn, you can use it

133
00:05:31,433 --> 00:05:33,133
as a windbreak and it's also nitrogen

134
00:05:33,133 --> 00:05:35,133
fixing as well. There's alders, there's

135
00:05:35,133 --> 00:05:37,766
native alders and there's near-native alders as

136
00:05:37,766 --> 00:05:41,333
well, the Italian Alder, so system plants

137
00:05:41,333 --> 00:05:43,033
that's a really good

138
00:05:43,033 --> 00:05:44,900
place to think about to use native plants.

139
00:05:44,900 --> 00:05:47,733
A lot of people moan

140
00:05:47,733 --> 00:05:50,666
about the limited palette of

141
00:05:50,800 --> 00:05:54,800
native plants and they've striving for this,

142
00:05:54,800 --> 00:05:55,666
their idea

143
00:05:55,666 --> 00:05:57,800
of what what their garden should look

144
00:05:57,800 --> 00:06:00,433
like but actually you need to revel

145
00:06:00,466 --> 00:06:02,666
in the vernacular and by this I

146
00:06:02,666 --> 00:06:04,966
mean, the vernacular in terms of

147
00:06:05,033 --> 00:06:08,066
architecture is the architecture that comes from

148
00:06:08,300 --> 00:06:11,533
a particular place. And it's the same

149
00:06:11,533 --> 00:06:13,966
with gardening, you really want to be

150
00:06:14,300 --> 00:06:15,766
looking at what is around you, what your

151
00:06:15,833 --> 00:06:18,666
environments are, what conditions you have

152
00:06:19,000 --> 00:06:21,400
what your soil is like, what climate

153
00:06:21,400 --> 00:06:23,500
you have and what grows there, what

154
00:06:23,500 --> 00:06:24,600
grows in these different conditions

155
00:06:24,600 --> 00:06:28,366
The main idea behind forest gardening is that you're

156
00:06:28,366 --> 00:06:30,066
working with nature and that's the same

157
00:06:30,066 --> 00:06:31,233
thing that you want to be doing

158
00:06:31,500 --> 00:06:33,533
with your native plants as well you

159
00:06:33,533 --> 00:06:35,266
want to work with the native plants

160
00:06:35,266 --> 00:06:36,000
that work best

161
00:06:36,000 --> 00:06:38,200
in your area. There's a really

162
00:06:38,200 --> 00:06:40,733
good resource for that called the

163
00:06:40,733 --> 00:06:43,466
Plant Atlas and it gives you a

164
00:06:43,466 --> 00:06:47,166
distribution map of the plants across

165
00:06:47,166 --> 00:06:48,966
the UK and Ireland. It is worth

166
00:06:48,966 --> 00:06:51,266
remembering as well about native plants, all

167
00:06:51,266 --> 00:06:53,900
plants are useful. Another great resource is

168
00:06:53,900 --> 00:06:56,166
Plants For A Future which is an

169
00:06:56,200 --> 00:06:58,300
online database of useful plants from around

170
00:06:58,300 --> 00:07:01,266
the world. There is practically every single

171
00:07:01,266 --> 00:07:03,166
UK native plant that I can

172
00:07:03,166 --> 00:07:05,300
think of has some sort of use

173
00:07:05,300 --> 00:07:07,533
to humans, one use or another

174
00:07:08,100 --> 00:07:10,533
and that's everything: edible plants, that's

175
00:07:10,533 --> 00:07:12,133
the herbal, you can use them for

176
00:07:12,133 --> 00:07:15,600
weaving baskets, for providing cordage, for ropes

177
00:07:15,800 --> 00:07:19,400
building materials, nutrients, all sorts of different things

178
00:07:20,266 --> 00:07:23,466
It's the wildlife food and habitat

179
00:07:23,500 --> 00:07:25,066
is part of that, you know

180
00:07:25,100 --> 00:07:26,866
we are not separate from nature

181
00:07:26,866 --> 00:07:28,733
we are part of nature, so by actually

182
00:07:28,733 --> 00:07:30,666
looking out for wildlife, it's

183
00:07:30,666 --> 00:07:34,433
useful for us as well and

184
00:07:34,800 --> 00:07:36,466
know your neighbours

185
00:07:36,466 --> 00:07:39,666
know which plants are feeding which insects

186
00:07:39,733 --> 00:07:41,233
for example here, if you press `p`

187
00:07:41,266 --> 00:07:43,000
there's a butterfly here, this is called

188
00:07:43,000 --> 00:07:46,066
a Map butterfly and the Nettle is

189
00:07:46,066 --> 00:07:48,066
the host plant and the Nettle as

190
00:07:48,066 --> 00:07:50,833
well, it has edible leaves, it's medicinal,

191
00:07:50,833 --> 00:07:52,866
it's a barrier plant, stops access

192
00:07:52,866 --> 00:07:55,000
to certain areas, you can use the

193
00:07:55,000 --> 00:07:56,533
fibres, I think you can use them

194
00:07:56,633 --> 00:07:58,966
for creating clothing, you can make leaf

195
00:07:58,966 --> 00:08:01,666
protein from it and yeah it's

196
00:08:01,666 --> 00:08:03,300
a great all round native plant.

197
00:08:03,300 --> 00:08:07,700
And, finally, cultivars. This is a Red Currant,

198
00:08:07,700 --> 00:08:12,200
Ribes rubrum, which is native, kind of

199
00:08:12,233 --> 00:08:14,400
to the UK but it has

200
00:08:14,400 --> 00:08:16,800
been cultivated, there have been cultivars

201
00:08:16,800 --> 00:08:19,066
grown from this and they're grown

202
00:08:19,066 --> 00:08:20,833
because you get better cropping from them

203
00:08:21,433 --> 00:08:23,833
A 'nativar' is a native cultivar

204
00:08:24,166 --> 00:08:26,000
so that's a bit of a mock term

205
00:08:26,766 --> 00:08:29,966
but generally cultivars are inferior in terms

206
00:08:29,966 --> 00:08:33,366
of wildlife and this really particularly pertains

207
00:08:33,366 --> 00:08:35,633
to the flowers because once you start

208
00:08:35,633 --> 00:08:36,799
breeding flowers

209
00:08:37,100 --> 00:08:40,033
in different colors and different patterns this

210
00:08:40,066 --> 00:08:44,100
really can detract from their attraction to

211
00:08:44,333 --> 00:08:46,233
pollinators for example

212
00:08:46,233 --> 00:08:47,766
And then the last

213
00:08:47,766 --> 00:08:50,700
thing I want us to discuss is neighbouring near-natives

214
00:08:50,700 --> 00:08:53,766
In the last ice age we had

215
00:08:53,766 --> 00:08:55,300
in the UK

216
00:08:55,333 --> 00:08:56,700
ten thousand years ago or so

217
00:08:57,366 --> 00:08:59,433
there's quite a few plants didn't get

218
00:08:59,433 --> 00:09:01,433
back across what is now the Channel

219
00:09:01,933 --> 00:09:04,166
and they are near-natives and that

220
00:09:04,166 --> 00:09:05,900
they're in parts of northern Europe but

221
00:09:05,900 --> 00:09:07,066
they didn't make it across over to

222
00:09:07,066 --> 00:09:09,833
the UK but those plants, particularly

223
00:09:09,833 --> 00:09:12,100
in the light of climate change, those

224
00:09:12,100 --> 00:09:15,533
plants would be quite useful to consider

225
00:09:15,800 --> 00:09:17,200
using, as temperatures get

226
00:09:17,266 --> 00:09:18,733
warmer and we want to be creating

227
00:09:18,733 --> 00:09:21,500
more resilient ecosystems and there will be

228
00:09:21,500 --> 00:09:24,200
some possible relationship between the wildlife and

229
00:09:24,233 --> 00:09:26,200
those native plants, so that's a whole field which

230
00:09:26,200 --> 00:09:28,666
is I think very much worth considering

231
00:09:28,700 --> 00:09:30,466
a bit beyond my remit

232
00:09:30,466 --> 00:09:32,766
as a gardener but definitely something

233
00:09:32,766 --> 00:09:35,200
to consider. So for example, here I'm

234
00:09:35,200 --> 00:09:37,433
growing Italian Alder which copes

235
00:09:37,466 --> 00:09:40,700
with dry conditions much better than the native Alder

236
00:09:40,700 --> 00:09:44,133
and it's a good nitrogen fixing plant

237
00:09:44,133 --> 00:09:46,466
for those very dry spots for a

238
00:09:46,700 --> 00:09:48,900
tall upright tree, where you wouldn't be

239
00:09:48,900 --> 00:09:50,100
able to fit in a native Alder

240
00:09:50,099 --> 00:09:54,399
So to round up, use native plants where possible

241
00:09:54,400 --> 00:09:57,133
wherever you can put a native plant in

242
00:09:57,133 --> 00:10:00,066
Okay, thank you very much for watching, cheers, bye

